const {src, dest, watch, parallel} = require("gulp");

//CSS
const sass = require("gulp-sass")(require('sass'));
const plumber = require('gulp-plumber');

//WEBP
const webp = require('gulp-webp');

//IMAGEMIN
const cache = require('gulp-cache');
const avif = require('gulp-avif');
const imagemin = require('gulp-imagemin');


function css (done) {
    src('src/scss/**/*.scss') // Identificar el archivo SASS
        .pipe( plumber())
        .pipe(sass()) // Compilarlo
        .pipe(dest ('build/css')) // Almacenarlo en el disco duro   
    
    done(); // Callback que avisa a gulp que llegamos al final de la funcion
}

function imagenes (done) {
    const opciones = {
        optimizationLevel: 3
    }

    src('src/img/**/*.{jpg, png}') // Le digo a la funcion donde buscar las imagenes
        .pipe(cache(imagemin(opciones)))
        .pipe(dest('build/img'))
    done();
}

function versionAvif (done) {

    const opciones = {
        quality: 50
    };

    src('src/img/**/*.{jpg, png}') // Le digo a la funcion donde buscar las imagenes
        .pipe(avif(opciones)) // Le aplico a estas imagenes la funcion "webp" para convertirlas
        .pipe(dest ('build/img')) // Almacenarlas en el disco duro   
    done();
}

function versionWebp (done) {

    const opciones = {
        quality: 50
    };

    src('src/img/**/*.{jpg, png}') // Le digo a la funcion donde buscar las imagenes
        .pipe(webp(opciones)) // Le aplico a estas imagenes la funcion "webp" para convertirlas
        .pipe(dest ("build/img")) // Almacenarlas en el disco duro   
    done();
}

function dev(done) {
    watch("src/scss/**/*.scss", css); // Mira si hay cambios en el archivo del primer parametro y, si hay cambios, ejecuta la funcion del segundo parametro 

    done(); 
}

exports.css = css;
exports.imagenes = imagenes;
exports.versionWebp = versionWebp;
exports.versionAvif = versionAvif;
exports.dev = parallel(versionWebp, imagenes, versionAvif, dev);